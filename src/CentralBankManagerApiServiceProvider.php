<?php
namespace Fjescala\CentralBank;

use Illuminate\Support\ServiceProvider;
use Fjescala\CentralBank\CentralBankManager;

class CentralBankManagerApiServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {
        //
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        $this->app['CentralBankApi'] = $this->app->singleton('Fjescala\CentralBank\Contracts\CentralBank', function($app) {
            return new CentralBankManager();
        });
    }

    /**
    * Get the functions provided 
    * @return [type] [description]
    */
    public function provides()
    {
        return ['Fjescala\CentralBank\Contracts\CentralBank'];
    }
}
