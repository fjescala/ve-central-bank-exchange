<?php
namespace Fjescala\CentralBank;

use Goutte\Client;
use Illuminate\Support\Collection;

/**
 * This class handle the request  
 */
class CentralBankManager 
{
    protected $url;
    protected $client;
    
    public function __construct()
    {
        $this->client = new Client();
        $this->url = 'http://www.bcv.org.ve/';
    
    }

    /**
     * Get currency 
     * @param  string $currency
     * @return mixed
     */
    public function getCurrency($currency)
    {
        switch ($currency) {
            case "dolar":
                   $crawler = $this->client->request('GET',$this->url);
                   $teste = $crawler->filter("div[id='dolar'] strong")->text();
                   return $teste;
                break;
            case "rublo":
                   $crawler = $this->client->request('GET', $this->url);
                   $teste = $crawler->filter("div[id='rublo'] strong")->text();
                   return $teste;      
                break;
            case "lira":
                $crawler = $this->client->request('GET', $this->url);
                $teste = $crawler->filter("div[id='lira'] strong")->text();
                return $teste;
                break;
            case "yuan":
                $crawler = $this->client->request('GET', $this->url);
                $teste = $crawler->filter("div[id='yuan'] strong")->text();
                return $teste;
                break;
            case "euro":
                $crawler = $this->client->request('GET', $this->url);
                $teste = $crawler->filter("div[id='euro'] strong")->text();
                return $teste;
                break;
          default:
                return "Moneda no Disponible."; 
              
                      }        
         
               
       
    
    }

 



 

}
