# Laravel 6-7-8-9 

package to extract the price of the different currencies by the central bank of Venezuela



## Installation


```
composer require fjescala/centralbank
```
## Uso



Here is a little example:

```php
use Fjescala\CentralBank\CentralBankManager as CentralBank; // at the top of the file

  public function __construct(CentralBank $central)
    {
        $this->central = $central;

 
    }

      public function index()
    {
        $amountInBolivares = $this->central->getCurrency("dolar");
        $amountInBolivares = $this->central->getCurrency("rublo");
        $amountInBolivares = $this->central->getCurrency("lira");
        $amountInBolivares = $this->central->getCurrency("yuan");
        $amountInBolivares = $this->central->getCurrency("euro");
       

      
    }
  
```




